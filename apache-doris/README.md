# Project README

This README provides a brief guide on setting up and running the project using Docker containers. It assumes you have Docker and Docker Compose installed on your system. If not, you can install them by following the official documentation for [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

### Adjust Kernel Parameters (Optional)
```bash
sysctl -w vm.max_map_count=${desiredValue}
```

## Step 1: Build Docker Image

### 1. Build Frontend (FE)

Make sure you have the frontend code ready and proceed with building the Docker image for the frontend. Replace `${frontendImageName}` with your desired image name.

```bash
docker build -t ${frontendImageName} ./frontend
```

### 2. Build Backend (BE)

```bash
docker build -t ${frontendImageName} ./backend
```

### 3. Build Broker

```bash
docker build -t ${frontendImageName} ./broker
```

## Step 2: Push Image to DockerHub

```bash
docker push ${TagName}
```

## Step 3: Running
### 1. Running Docker Container
```bash
docker-compose up 
```