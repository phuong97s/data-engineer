import os
from dotenv import load_dotenv
import psycopg2
import logging 
import requests
from requests.exceptions import HTTPError, RequestException, Timeout
import pandas as pd

# Load the environment variables
load_dotenv()
API_KEY         =   os.getenv("API_KEY")
API_HOST        =   os.getenv("API_HOST")
DB_NAME         =   os.getenv("DB_NAME")
DB_USERNAME     =   os.getenv("DB_USERNAME")
DB_PASSWORD     =   os.getenv("DB_PASSWORD")
DB_HOST         =   os.getenv("DB_HOST")
DB_PORT         =   os.getenv("DB_PORT")

# Set up the logger 
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')

# Create a file handler
file_handler = logging.FileHandler('football_table_standings.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

# Create a console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

# Instantiate the logger object
logger = logging.getLogger()

# Add the file handler to the logger
logger.addHandler(file_handler)

# Add the console handler to the logger
logger.addHandler(console_handler)

# https://rapidapi.com/movie-of-the-night-movie-of-the-night-default/api/streaming-availability/
url           =   "https://streaming-availability.p.rapidapi.com/search/filters"
headers       =   {"X-RapidAPI-Key": API_KEY, "X-RapidAPI-Host": API_HOST}
querystring = {"services":"netflix","country":"us","output_language":"en","order_by":"original_title","genres_relation":"and","show_type":"all"}

try:
    api_response = requests.get(url, headers=headers, params=querystring, timeout=15)
    api_response.raise_for_status() 

except HTTPError as http_err:
    logger.error(f'HTTP error occurred: {http_err}')

except Timeout:
    logger.error('Request timed out after 15 seconds')

except RequestException as request_err:
    logger.error(f'Request error occurred: {request_err}')

result_list = api_response.json()['result']

# List to hold our extracted data
data_list = []
movie_id = 0

for result in result_list:
    # Example: Accessing information for the first movie
    movie_id += 1
    movie_type = result['type']
    movie_title = result['title']    
    movie_service = result['streamingInfo']['us'][0]['service']
    movie_link = result['streamingInfo']['us'][0]['link']
    movie_imdbId = result['imdbId']
    movie_genres = result['genres'][0]
    movie_director = result_list[0]['directors'][0]

    data_list.append([movie_id, movie_type, movie_title, movie_service, movie_link, movie_imdbId, movie_genres, movie_director])
  
# Create the dataFrame
columns         =   ['id', 'type', 'title', 'service', 'link', 'imdbId', 'genres', 'director']
movies_df    =   pd.DataFrame(data_list, columns=columns)

print(movies_df.to_string(index=False))

# Set up Postgres database connection
postgres_connection = psycopg2.connect(
    dbname=DB_NAME,
    user=DB_USERNAME,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT
)

# Get a cursor from the database
cur = postgres_connection.cursor()

# Use SQL to create a table for the Premier League 
create_table_sql_query = """
    CREATE TABLE IF NOT EXISTS netflix (
            id                  INT PRIMARY KEY,
            type                VARCHAR(255),
            title               VARCHAR(255),
            service             VARCHAR(255),
            link                VARCHAR(255),
            imdbId              VARCHAR(255),
            genre               VARCHAR(255),
            director            VARCHAR(255)
    );
"""
# Run the SQL query 
cur.execute(create_table_sql_query)


# Commit the transaction 
postgres_connection.commit()


# Use SQL to insert data into the Premier League table 
insert_data_sql_query = """
    INSERT INTO public.netflix (
            id, type, title, service, link, imdbId, genre, directors            
    )
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)

    ON CONFLICT (position) DO UPDATE SET
    type              =   EXCLUDED.type, 
    title             =   EXCLUDED.title, 
    service           =   EXCLUDED.service, 
    link              =   EXCLUDED.link, 
    imdbId            =   EXCLUDED.imdbId, 
    genre             =   EXCLUDED.genre, 
    director          =   EXCLUDED.director, 
"""



for idx, row in movies_df.iterrows():
    cur.execute(insert_data_sql_query, 
                (row['id'], 
                 row['type'], 
                 row['title'], 
                 row['service'], 
                 row['link'], 
                 row['imdbId'], 
                 row['genre'], 
                 row['director']
                 )
    )

# Commit the transaction 
postgres_connection.commit()
