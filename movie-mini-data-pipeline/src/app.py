import os
import psycopg2
import pandas as pd 
from PIL import Image
import streamlit as st
import plotly.express as px
from dotenv import load_dotenv

# Load the environment variables 
load_dotenv()

DB_NAME         =   os.getenv("DB_NAME")
DB_USERNAME     =   os.getenv("DB_USERNAME")
DB_PASSWORD     =   os.getenv("DB_PASSWORD")
DB_HOST         =   os.getenv("DB_HOST")
DB_PORT         =   os.getenv("DB_PORT")



# Set up Postgres database connection
postgres_connection = psycopg2.connect(
    dbname=DB_NAME,
    user=DB_USERNAME,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT
)

# Get a cursor from the database
cur = postgres_connection.cursor()

# Fetch the Premier League data from Postgres
netflix_sql_query = """
    SELECT 
        *
    FROM 
        public.netfix
"""

# Read football data into dataframe
netflix_df = pd.read_sql(netflix_sql_query, postgres_connection)


# Remove the index displayed
netflix_df.set_index('position', inplace=True)

# Close database connection
postgres_connection.close()